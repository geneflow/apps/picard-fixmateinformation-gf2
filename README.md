Picard FixMateInformation GeneFlow App
======================================

Version: 2.22.7-01

This GeneFlow app wraps the Picard FixMateInformation tool.

Inputs
------

1. input: BAM File - An alignment BAM file.

Parameters
----------

1. validation_stringency: Stringency of Validation (for SAM), default: lenient

2. output: Output directory - The name of the output directory, where the BAM file is stored. Default: output

